class Holiday {
    constructor({ name, irrenunciable, daysUntil, longWeekend }) {
      this.name = name;
      this.irrenunciable = irrenunciable;
      this.daysUntil = daysUntil;
      this.longWeekend = longWeekend;
    }
  
    toJSON() {
      return {
        name: this.name,
        irrenunciable: this.irrenunciable,
        daysUntil: this.daysUntil,
        longWeekend: this.longWeekend
      };
    }
  }
  
  module.exports = Holiday;
  