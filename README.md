# API para verificar feriados en Chile

Este proyecto implementa una API en Node.js utilizando Express y Serverless Framework para verificar si un día específico es feriado en Chile utilizando la API de Feriados legales.

## Instalación

1. Clonar el repositorio.
2. Instalar las dependencias: `npm install`.
3. Para probar locar se debe ejecutar el siguiente comando: `serverless offline start`.

# Arquitectura del proyecto
 - Se usa una aquitectura serverless utilizando los servicios de aws como Lambda, Api Gateway y CloudWatch
  - Se implementó un patrón de arquitectura MVC (modelo, vista y controlador) y también se utilizó unos middlewares de validacion de campos y de error handler.
  - En la carpeta feature se creó un archivo gherkin .feature en donde estan descritos todos los escenarios de los casos pruebas de sistema.
  Se adjunta de igu al forma un archivo que contiene los reportes de la ejecución de dichas pruebas. (Solo pruebas manuales en honor al tiempo)


## Uso
 ``` json
 curl --location 'http://localhost:3000/api/check-holiday' \
--header 'Content-Type: application/json' \
--data '{
  "year": 2024,
  "month": 1,
  "day": 11
}
'
```
### Endpoint

- `POST /api/check-holiday`

#### Parámetros de entrada

```json
{
  "year": 2024,
  "month": 9,
  "day": 18
}
```

### Descripción de los campos:

- isHoliday: Este campo indica si la fecha ingresada es un día feriado. Su valor es un booleano que puede ser `true` (verdadero) si es feriado o `false` (falso) si no lo es.

- holiday: Este es un objeto que contiene detalles sobre el feriado en caso de que el campo isHoliday sea true.

- name: Este campo es una cadena de texto que contiene el nombre del feriado. En el ejemplo proporcionado, es `"San Pedro y San Pablo"`.

- irrenunciable: Este campo es un booleano que indica si el feriado es irrenunciable, lo que significa que los trabajadores no están obligados a trabajar en ese día. Su valor puede ser `true` (verdadero) o `false` (falso).

- daysUntil: Este campo es un número entero que indica cuántos días faltan para que se celebre el feriado desde la fecha ingresada.

- longWeekend: Este campo puede ser null o un objeto que contiene detalles sobre si el feriado genera un fin de semana largo. Si el valor es `null`, significa que no hay fin de semana largo asociado con el feriado.
## TEST

. Para las pruebas unitarias se utilizó unas librerías de jsvascript llamadas `supertes` & `jest`

. Para ejecutar los tests se debe ejecutar el siguiente comando: `npx jest`


### Resultado de los Units Tests

![test results](image.png)


# DETALLE DE DESPLIEGUE 

El proyecto esta hecho para ser desplegado en la nube de AWS en la region`us-east-1`. Pero al despligar el proyecto final se me presentó un el siguiente error: `EMFILE: too many open files` ![error deploy](image-1.png)
Estoy trantado de solucionarlo aun no lo he logrado, pero ya esta todo setado para su despliegue, de hecho tengo una version de prueba ya desplegada en aws, puedes acceder a través de la apigateway: `https://ylzga4msoc.execute-api.us-east-1.amazonaws.com` y path: `/hello`

se puede observar el proyecto en la web de serverless framework
![proyecto](image-2.png)
