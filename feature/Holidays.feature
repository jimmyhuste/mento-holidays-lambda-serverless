Feature: Verificar feriados en Chile

  Scenario: TS001 - Devolver un feriado cuando existe
    Given Que el ano es 2024 el mes es 1 y el dia es 1
    When Realizo una solicitud POST a "/api/check-holiday"
    Then Se debe visualizar codigo de estado "200"
    And Valido respuesta debe tener "isHoliday" igual a "true"
    And Valido respuesta debe tener "name" igual a "Año Nuevo"
    And Valido respuesta debe tener "irrenunciable" igual a "true"
    And Valido respuesta debe tener "daysUntil" igual a un "-176"
    And Valido respuesta debe tener "longWeekend" igual a "null"

    """
    REQUEST: 
    {
      "year": 2024,
      "month": 6,
      "day": 29
    }

    RESPONSE JSON:
    {
      "isHoliday": true,
      "holiday": {
          "name": "Año Nuevo",
          "irrenunciable": true,
          "daysUntil": -176,
          "longWeekend": null
      }
  }
    """

  Scenario: TS002 -Validar dia mensaje cuando no es feriado
    Given Que el ano es "2024" el mes es "7" y el día es "27"
    When realizo una solicitud POST a "/api/check-holiday"
    Then Se debe visualizar codigo de estado "200"
    And Valido respuesta debe tener "isHoliday" igual a "false"
    And Valido respuesta debe tener "mensaje" igual a "No se han encontrado feriados"

  Scenario: TS003 - Validar error cuando el dia es nulo
    Given que el ano es "2024" el mes es "9" y el dia es "41"
    When realizo una solicitud POST a "/api/check-holiday"
    Then Se debe visualizar codigo de estado "400"
    And Valido respuesta debe tener "message" igual a "Error campo invalido"


  Scenario: TS004 - Validar error cuando el mes es nulo
    Given Que el ano es null el mes es 1 y el dia es 1
    When Realizo una solicitud POST a "/api/check-holiday"
    Then Se debe visualizar codigo de estado "400"
    And Valido respuesta debe tener "message" igual a "El ano es requerido y debe ser un entero entre 1900 y 2100"

  Scenario: TS005 - Validar error cuando el ano esta fuera del rango
    Given Que el ano es 1800 el mes es 1 y el dia es 1
    When Realizo una solicitud POST a "/api/check-holiday"
    Then Se debe visualizar codigo de estado "400"
    And Valido respuesta debe tener "message" igual a "El ano es requerido y debe ser un entero entre 1900 y 2100"

  Scenario: TS006 - Validar error cuando el mes esta fuera del rango
    Given Que el ano es 2024 el mes es 13 y el dia es 1
    When Realizo una solicitud POST a "/api/check-holiday"
    Then Se debe visualizar codigo de estado "400"
    And Valido respuesta debe tener "message" igual a "El mes es requerido y debe ser un entero entre 1 y 12"

  Scenario: TS007 - Validar error cuando el dia esta fuera del rango
    Given Que el ano es 2024 el mes es 1 y el dia es 32
    When Realizo una solicitud POST a "/api/check-holiday"
    Then Se debe visualizar codigo de estado "400"
    And Valido respuesta debe tener "message" igual a "El dia es requerido y debe ser un entero entre 1 y 31"
