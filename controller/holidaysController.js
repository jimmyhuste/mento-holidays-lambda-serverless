const axios = require('axios');
const moment = require('moment-timezone');
const Holiday = require('../model/holiday');
const { validationResult } = require('express-validator');

// Función para verificar si un día es feriado en Chile
exports.checkHoliday = async (req, res, next) => {

   // Verificar si hay errores de validación
   const errors = validationResult(req);
   if (!errors.isEmpty()) {
     return res.status(400).json({ errors: errors.array() });
   }
  const { year, month, day } = req.body;

  const API_URL = 'https://apis.digital.gob.cl/fl/feriados';

  try {
    const response = await axios.get(`${API_URL}/${year}/${month}/${day}`);
    const holidayData = response.data;
    console.log(holidayData)
    // Se verifica si holidayData es un array y tiene al menos un elemento
    if (Array.isArray(holidayData) && holidayData.length > 0) {
      let foundHoliday = false;

      // Se recorre cada objeto en el array
      holidayData.forEach((holiday) => {
        if (holiday.nombre) {
          foundHoliday = true;
          const date = moment.tz(holiday.fecha, 'YYYY-MM-DD', 'America/Santiago');
          const daysUntil = date.diff(moment(), 'days');
          const isLongWeekend = holiday.comentarios && holiday.comentarios.includes('fin de semana largo');

          const holidayInstance = new Holiday({
            name: holiday.nombre,
            irrenunciable: holiday.irrenunciable === '1',
            daysUntil: daysUntil,
            longWeekend: isLongWeekend
          });

          // Responde con el primer feriado encontrado
          return res.status(200).json({
            isHoliday: true,
            holiday: holidayInstance.toJSON()
          });
        }
      });

      // Si no se encontró ningún feriado con nombre
      if (!foundHoliday) {
        return res.status(200).json({
          isHoliday: false,
          mensaje : 'No se encontró ningún feriado para ese día.'
        });
      }
    } else {
      return res.status(200).json({
        isHoliday: false,
      });
    }
  } catch (error) {
    console.error('Error al consultar API de feriados:', error);
    return res.status(500).json({
      error: true,
      message: 'Error al consultar API de feriados'
    });
  }
};
