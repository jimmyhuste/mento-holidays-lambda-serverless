const express = require('express');
const router = express.Router();
const holidaysController = require('../controller/holidaysController');
const { validateHolidayRequest } = require('../middleware/validator/checkHolidayValidator');


// Ruta para verificar si un día es feriado en Chile
router.post('/check-holiday', validateHolidayRequest, holidaysController.checkHoliday);

module.exports = router;