const serverless = require('serverless-http');
const express = require('express');
const app = express();
const holidayRoutes = require('./routes/holidaysRoutes.routes');
const errorHandler = require('./middleware/errorHandler');

app.use(express.json());

// Rutas de la aplicación
app.use('/api', holidayRoutes);

// Middleware para manejar errores
app.use(errorHandler);

// Se exporto app para pruebas unitarias
module.exports = app;
// Exportar la aplicación adaptada para AWS Lambda
module.exports.handler = serverless(app);

