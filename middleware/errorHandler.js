const errorHandler = (error, req, res, next) => {
    console.error('Error en la solicitud:', error);
  
    const statusCode = res.statusCode !== 200 ? res.statusCode : 500;
    res.status(statusCode).json({
      error: error.message
    });
  };
  
  module.exports = errorHandler;
  