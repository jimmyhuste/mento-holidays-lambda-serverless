const { check, validationResult } = require('express-validator');

// Middleware para validar los campos year, month y day
exports.validateHolidayRequest = [
  // Validación del año
  check('year', 'El año es requerido y debe ser un entero entre 1900 y 2100')
    .notEmpty().withMessage('El año es requerido')
    .isInt({ min: 1900, max: 2100 }).withMessage('El año debe ser un entero entre 1900 y 2100')
    .custom(value => {
      if (value === null) {
        throw new Error('El año no puede ser null');
      }
      return true;
    }),

  // Validación del mes
  check('month', 'El mes es requerido y debe ser un entero entre 1 y 12')
    .notEmpty().withMessage('El mes es requerido')
    .isInt({ min: 1, max: 12 }).withMessage('El mes debe ser un entero entre 1 y 12')
    .custom(value => {
      if (value === null) {
        throw new Error('El mes no puede ser null');
      }
      return true;
    }),

  // Validación del día
  check('day', 'El día es requerido y debe ser un entero entre 1 y 31')
    .notEmpty().withMessage('El día es requerido')
    .isInt({ min: 1, max: 31 }).withMessage('El día debe ser un entero entre 1 y 31')
    .custom(value => {
      if (value === null) {
        throw new Error('El día no puede ser null');
      }
      return true;
    }),
];
