const request = require('supertest');
const app = require('../handler');

/**
 * Testing POST /api/check-holiday endpoint
 */
describe("POST /api/check-holiday", () => {

  it("Debería devolver un feriado cuando existe", (done) => {
    const requestBody = {
      year: 2024,
      month: 1,
      day: 1
    };

    request(app)
      .post('/api/check-holiday')
      .send(requestBody)
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .expect(200)
      .expect((response) => {
        expect(response.body.isHoliday).toBe(true);
        expect(response.body.holiday).toHaveProperty('name', 'Año Nuevo');
        expect(response.body.holiday).toHaveProperty('irrenunciable', true);
        expect(response.body.holiday).toHaveProperty('daysUntil', expect.any(Number));
        expect(response.body.holiday).toHaveProperty('longWeekend', null);
      })
      .end((err) => {
        if (err) return done(err);
        done();
      });
  });

  it("Debería devolver un mensaje cuando no es feriado", (done) => {
    const requestBody = {
      year: 2024,
      month: 6,
      day: 27
    };

    request(app)
      .post('/api/check-holiday')
      .send(requestBody)
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .expect(200)
      .expect((response) => {
        expect(response.body.isHoliday).toBe(false);
      })

      .end((err) => {
        if (err) return done(err);
        done();
      });
  });




});

describe("POST /api/check-holiday", () => {
  it("Debería devolver un error 400 en caso de campos inválidos", (done) => {
    const requestBody = {
      year: 2024,
      month: 9,
      day: 41
    };

    request(app)
      .post('/api/check-holiday')
      .send(requestBody)
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .expect(400)
      .expect((response) => {
        expect(response.body.errors);
      })

      .end((err) => {
        if (err) return done(err);
        done();
      });
  });

});